package com.example.promittest.views.base

interface BaseActivityInterface{
    fun setErrorInView(errorMessage: String?)

    fun hiddenProgressBar()

    fun showProgressBar()
}