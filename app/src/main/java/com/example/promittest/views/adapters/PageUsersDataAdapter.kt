package com.example.promittest.views.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.promittest.R
import com.example.promittest.domain.Entities.UserDataDomain
import com.example.promittest.views.presenters.UserListActivityUpdateInterface
import kotlinx.android.synthetic.main.cart_user.view.*

class PageUsersDataAdapter (items: ArrayList<UserDataDomain>, private val view: UserListActivityUpdateInterface) : RecyclerView.Adapter<PageUsersDataAdapter.ViewHolder>(){
    private val mItems : ArrayList<UserDataDomain> = items
    private var lastPositionItem = 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.textViewFirstName.firstName.text = mItems[position].firstName
        holder.textViewLastName.lastName.text = mItems[position].lastName
        holder.textViewEmail.email.text = mItems[position].email
        Glide.with(holder.itemView)
            .load(mItems[position].avatarUrl)
            .into(holder.image.avatar)
        if (position == mItems.count() - 1)
        {
            lastPositionItem = position
            view.loadNextPage()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cart_user, parent, false)

        return ViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return mItems.count()
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        var textViewFirstName = view.firstName!!
        var textViewLastName = view.lastName!!
        var textViewEmail = view.email!!
        var image = view.avatar!!
    }

    fun getUserId(position: Int): Int{
        return mItems[position].id
    }

    fun getLastPositionVisibleItem(): Int{
        return lastPositionItem
    }

    fun updateData(items: ArrayList<UserDataDomain>){
        mItems.addAll(items)
    }
}