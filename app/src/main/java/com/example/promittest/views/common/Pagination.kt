package com.example.promittest.views.common

class Pagination ()
{
    private var firstPageNum: Int = 1
    private var currentPageNum: Int = 1

    constructor(
        firstPageNum: Int,
        currentPageNum: Int
    ) : this(){
        this.firstPageNum = firstPageNum
        this.currentPageNum = currentPageNum
    }

    constructor(
        currentPageNum: Int
    ) : this(){
        this.currentPageNum = currentPageNum
    }

    fun getCurrentPageNum(): Int{
        return currentPageNum
    }

    fun getNextPageNum(): Int{
        return currentPageNum + 1
    }

    fun getFirstPage(): Int{
        return firstPageNum
    }

    fun incrementPageNum(){
        currentPageNum++
    }
}