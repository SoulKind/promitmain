package com.example.promittest.views.base

import android.os.Bundle
import android.widget.Toast

import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity : AppCompatActivity(),
    BaseActivityInterface {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setErrorInView(text: String?){
        Toast.makeText(
            applicationContext,
            text,
            Toast.LENGTH_LONG
        ).show()
    }
}