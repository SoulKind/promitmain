package com.example.promittest.views.presenters

import com.example.promittest.views.base.BaseActivityInterface

interface UserListActivityUpdateInterface : BaseActivityInterface {
    fun updateUserList()

    fun loadNextPage()
}