package com.example.promittest.views.presenters

import com.example.promittest.data.SearchRepository
import com.example.promittest.data.SearchRepositoryProvider
import com.example.promittest.domain.Entities.PageDomain
import com.example.promittest.domain.Entities.UserDataDomain
import com.example.promittest.views.common.Pagination
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UserListPresenter(private val view: UserListActivityUpdateInterface) {
    private val repository: SearchRepository = SearchRepositoryProvider.provideSearchRepository()
    private var totalPage: Int = 0
    private val pageDomainList: ArrayList<PageDomain> = arrayListOf()
    lateinit var currentPageApiEntity: PageDomain
    private var usersDomain: ArrayList<UserDataDomain> = arrayListOf()
    private val paginationPage = Pagination()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    init{
        compositeDisposable.add(
            repository.getPageByNum(paginationPage.getFirstPage())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        s -> pageDomainList.add(s)
                            totalPage = s.totalPages
                            currentPageApiEntity = s
                            usersDomain = s.users
                            view.updateUserList()
                            view.hiddenProgressBar()
                    },
                    {
                        view.hiddenProgressBar()
                        view.setErrorInView(it.message)
                    }
                )
        )
    }

    fun  getUsers(): ArrayList<UserDataDomain> {
        return usersDomain
    }

    fun loadNextPage(){
        if(paginationPage.getNextPageNum() > totalPage){
            return
        }

        view.showProgressBar()
        compositeDisposable.add(
            repository.getPageByNum(paginationPage.getNextPageNum())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(
                    {
                        result ->
                            pageDomainList.add(result)
                            paginationPage.incrementPageNum()
                            currentPageApiEntity = result
                            usersDomain = result.users
                            view.updateUserList()
                            view.hiddenProgressBar()
                    },
                    {
                        view.hiddenProgressBar()
                        view.setErrorInView(it.message)
                    }
                )
        )
    }
}