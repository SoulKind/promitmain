package com.example.promittest.views.presenters

import com.example.promittest.views.base.BaseActivityInterface

interface UserUpdateInterface : BaseActivityInterface {
    fun updateUser()
}