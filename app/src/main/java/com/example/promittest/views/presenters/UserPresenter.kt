package com.example.promittest.views.presenters

import com.example.promittest.data.SearchRepository
import com.example.promittest.data.SearchRepositoryProvider
import com.example.promittest.domain.Entities.UserDomain
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UserPresenter(private val view: UserUpdateInterface){
    private lateinit var selectedUserEntity: UserDomain
    private val repository: SearchRepository = SearchRepositoryProvider.provideSearchRepository()
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun getSelectedUser(): UserDomain {
        return selectedUserEntity
    }

    fun loadSelectedUser(userId: Int){
        compositeDisposable.add(
            repository.getUserById(userId)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe (
                    {
                        result -> selectedUserEntity = result
                        view.updateUser()
                        view.hiddenProgressBar()
                    },
                    {
                        view.hiddenProgressBar()
                        view.setErrorInView(it.message)
                    }
                )
        )
    }
}