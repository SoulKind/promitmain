package com.example.promittest.views
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.promittest.R
import com.example.promittest.views.presenters.UserListActivityUpdateInterface
import com.example.promittest.views.listeners.RecyclerItemClickListener
import com.example.promittest.views.adapters.PageUsersDataAdapter
import com.example.promittest.views.base.BaseActivity
import com.example.promittest.views.listeners.RecyclerItemClickListener.*
import com.example.promittest.views.presenters.UserListPresenter
import kotlinx.android.synthetic.main.activity_user_list.*


const val tag = "MainActivity"

class UserListActivity : BaseActivity(),
    UserListActivityUpdateInterface {
    lateinit var userListPresenter: UserListPresenter
    var loading = true

    lateinit var pageUsersDataAdapter: PageUsersDataAdapter
    lateinit var llm: LinearLayoutManager

    var pastVisiblesItems = 0
    var visibleItemCount = 0
    var totalItemCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_list)

        llm = LinearLayoutManager(this.baseContext)
        llm.orientation = LinearLayoutManager.VERTICAL
        usersRecyclerView.layoutManager = llm

        userListPresenter =
            UserListPresenter(this)

        pageUsersDataAdapter =
            PageUsersDataAdapter(userListPresenter.getUsers(), this)

        usersRecyclerView.adapter = pageUsersDataAdapter

        this.usersRecyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0)
                {
                    visibleItemCount = llm.childCount
                    totalItemCount = llm.itemCount
                    pastVisiblesItems = llm.findFirstVisibleItemPosition()
                    if (loading) {
                        if (visibleItemCount + pastVisiblesItems >= totalItemCount) {
                            loading = false

                            userListPresenter.loadNextPage()
                        }
                    }
                }
            }
        })

        usersRecyclerView.addOnItemTouchListener(
            RecyclerItemClickListener(
                usersRecyclerView,
                object :
                    OnItemClickListener {
                    override fun onItemClick(view: View, position: Int) {

                        val userId =
                            (usersRecyclerView.adapter as PageUsersDataAdapter).getUserId(position)
                        setErrorInView(userId.toString())

                        val intent =
                            Intent(this@UserListActivity, UserActivity::class.java)
                        intent.putExtra("userId", userId)
                        startActivity(intent)
                    }
                })
        )
    }

    override fun updateUserList() {
        (usersRecyclerView.adapter as PageUsersDataAdapter).updateData(userListPresenter.getUsers())
        (usersRecyclerView.adapter as PageUsersDataAdapter).notifyDataSetChanged()
    }


    override fun loadNextPage(){
        userListPresenter.loadNextPage()
    }

    override fun hiddenProgressBar() {
        progressBarUserList.isVisible = false
    }

    override fun showProgressBar() {
        progressBarUserList.isVisible = true
    }
}