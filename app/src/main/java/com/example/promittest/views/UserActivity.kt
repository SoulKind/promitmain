package com.example.promittest.views

import android.os.Bundle
import android.view.MenuItem
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.example.promittest.R
import com.example.promittest.views.base.BaseActivity
import com.example.promittest.views.presenters.UserPresenter
import com.example.promittest.views.presenters.UserUpdateInterface
import kotlinx.android.synthetic.main.activity_user.*


class UserActivity : BaseActivity(),
    UserUpdateInterface {
    lateinit var presenter: UserPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user)

        supportActionBar?.setDisplayShowTitleEnabled(false)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val userId: Int = intent.getIntExtra("userId", 0)
        presenter = UserPresenter(this)
        presenter.loadSelectedUser(userId)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> finish()
        }
        return true
    }

    override fun updateUser() {
        val result = presenter.getSelectedUser()
        firstNameUser.text = result.userData.firstName
        lastNameUser.text = result.userData.lastName
        emailUser.text = result.userData.email
        textCompanyUser.text = result.userCompany.text
        urlUser.text = result.userCompany.url
        companyUser.text = result.userCompany.company
        Glide.with(this)
            .load(result.userData.avatarUrl)
            .into(avatarUser)
    }

    override fun hiddenProgressBar() {
        progressBarUser.isVisible = false
    }

    override fun showProgressBar() {
        progressBarUser.isVisible = true
    }
}