package com.example.promittest.domain.Entities

class PageDomain(
    val pageNumber: Int,
    val perPage: Int,
    val total: Int,
    val totalPages: Int,
    val users: ArrayList<UserDataDomain>,
    val company: UserCompanyDomain
)