package com.example.promittest.domain.Entities

import android.media.Image

class UserDataDomain (
    val id: Int,
    val email: String,
    val firstName: String,
    val lastName: String,
    val avatarUrl: String,
    val avatar: Image?
)