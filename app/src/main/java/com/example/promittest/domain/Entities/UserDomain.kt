package com.example.promittest.domain.Entities

data class UserDomain(
    val userData: UserDataDomain,
    val userCompany: UserCompanyDomain
)