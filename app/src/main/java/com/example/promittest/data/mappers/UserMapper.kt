package com.example.promittest.data.mappers

import com.example.promittest.data.apiEntities.UserCompanyApiEntity
import com.example.promittest.data.apiEntities.UserDataApiEntity
import com.example.promittest.data.apiEntities.UserApiEntity
import com.example.promittest.data.base.BaseMapper
import com.example.promittest.domain.Entities.UserCompanyDomain
import com.example.promittest.domain.Entities.UserDataDomain
import com.example.promittest.domain.Entities.UserDomain

class UserMapper() : BaseMapper<UserApiEntity, UserDomain>() {

    override fun map(el: UserApiEntity): UserDomain {
        return UserDomain(
            userData = UserDataDomain(
                el.userData.id,
                el.userData.email,
                el.userData.firstName,
                el.userData.lastName,
                el.userData.avatarUrl,
                null
            ),
            userCompany = UserCompanyDomain(
                el.userCompany.company,
                el.userCompany.url,
                el.userCompany.text
            )
        )
    }

    override fun reverseMap(el: UserDomain): UserApiEntity {
       return UserApiEntity(
           UserDataApiEntity(
               el.userData.id,
               el.userData.email,
               el.userData.firstName,
               el.userData.lastName,
               el.userData.avatarUrl
           ),
           UserCompanyApiEntity(
               el.userCompany.company,
               el.userCompany.url,
               el.userCompany.text
           )
       )
    }


}