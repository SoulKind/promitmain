package com.example.promittest.data.apiEntities

import com.google.gson.annotations.SerializedName

data class PageApiEntity(
    @SerializedName("page") val pageNumber: Int,
    @SerializedName("per_page") val perPage: Int,
    @SerializedName("total") val total: Int,
    @SerializedName("total_pages") val totalPages: Int,
    @SerializedName("data") val users: ArrayList<UserDataApiEntity>,
    @SerializedName("ad") val userCompanyApiEntity: UserCompanyApiEntity
)