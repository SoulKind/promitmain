package com.example.promittest.data

import com.example.promittest.data.api.BashImApiService

object SearchRepositoryProvider {
    fun provideSearchRepository(): SearchRepository {
        return SearchRepository(BashImApiService.create())
    }
}