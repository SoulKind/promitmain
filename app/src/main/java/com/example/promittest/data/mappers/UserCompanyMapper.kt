package com.example.promittest.data.mappers

import com.example.promittest.data.base.BaseMapper
import com.example.promittest.data.apiEntities.UserCompanyApiEntity
import com.example.promittest.domain.Entities.UserCompanyDomain

class UserCompanyMapper : BaseMapper<UserCompanyApiEntity, UserCompanyDomain>(){
    override fun map(el: UserCompanyApiEntity): UserCompanyDomain {
        return UserCompanyDomain(
            el.company,
            el.url,
            el.text
        )
    }

    override fun reverseMap(el: UserCompanyDomain): UserCompanyApiEntity {
        return UserCompanyApiEntity(
            el.company,
            el.url,
            el.text
        )
    }

}