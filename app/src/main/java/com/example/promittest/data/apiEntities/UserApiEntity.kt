package com.example.promittest.data.apiEntities

import com.google.gson.annotations.SerializedName

data class UserApiEntity (
    @SerializedName("data") val userData: UserDataApiEntity,
    @SerializedName("ad") val userCompany: UserCompanyApiEntity
)