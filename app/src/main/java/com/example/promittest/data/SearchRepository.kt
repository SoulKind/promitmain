package com.example.promittest.data

import com.example.promittest.data.api.BashImApiService
import com.example.promittest.data.mappers.PageMapper
import com.example.promittest.data.mappers.UserMapper
import com.example.promittest.domain.Entities.PageDomain
import com.example.promittest.domain.Entities.UserDomain
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SearchRepository  (private val bashImApiService: BashImApiService) {
    private val userApiMapper = UserMapper()
    private val pageMapper = PageMapper()

    fun getUserById(id: Int): Single<UserDomain> {
        return bashImApiService.getUserById(id)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .map(userApiMapper::map)
    }

    fun getPageByNum(num: Int): Single<PageDomain>{
        return bashImApiService.getPageByNum(num)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .map(pageMapper::map)
    }
}

