package com.example.promittest.data.mappers

import com.example.promittest.data.apiEntities.PageApiEntity
import com.example.promittest.data.base.BaseMapper
import com.example.promittest.domain.Entities.PageDomain

class PageMapper : BaseMapper<PageApiEntity, PageDomain>(){
    private val userDateMapper = UserDataMapper()
    private val userCompanyMapper = UserCompanyMapper()

    override fun map(el: PageApiEntity): PageDomain {
        return PageDomain(
            pageNumber = el.pageNumber,
            perPage = el.perPage,
            total = el.total,
            totalPages = el.totalPages,
            users = userDateMapper.map(el.users),
            company = userCompanyMapper.map(el.userCompanyApiEntity)
        )
    }

    override fun reverseMap(el: PageDomain): PageApiEntity {
        return PageApiEntity(
            pageNumber = el.pageNumber,
            perPage = el.perPage,
            total = el.total,
            totalPages = el.totalPages,
            users = userDateMapper.reverseMap(el.users),
            userCompanyApiEntity = userCompanyMapper.reverseMap(el.company)
        )
    }
}