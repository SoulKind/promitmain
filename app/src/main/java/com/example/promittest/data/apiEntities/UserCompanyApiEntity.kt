package com.example.promittest.data.apiEntities

import com.google.gson.annotations.SerializedName

data class UserCompanyApiEntity(
    @SerializedName("company") val company: String,
    @SerializedName("url") val url: String,
    @SerializedName("text") val text: String
)