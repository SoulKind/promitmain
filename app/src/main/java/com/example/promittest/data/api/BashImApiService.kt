package com.example.promittest.data.api


import com.example.promittest.data.apiEntities.PageApiEntity
import com.example.promittest.data.apiEntities.UserApiEntity
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BashImApiService{
    @GET("api/users/{id}")
    fun getUserById(
        @Path("id") id: Int
    ): Single<UserApiEntity>

    @GET("api/users?")
    fun getPageByNum(
        @Query("page") num: Int
    ): Single<PageApiEntity>

    companion object Factory{
        fun create(): BashImApiService {
            val gson:Gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl("https://reqres.in/")
                .build()
            return retrofit.create(BashImApiService::class.java)
        }
    }
}