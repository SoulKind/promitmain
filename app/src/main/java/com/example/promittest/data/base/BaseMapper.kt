package com.example.promittest.data.base

abstract class BaseMapper<T, K> {
    abstract fun map(el: T): K

    abstract fun reverseMap(el: K): T

    fun map(list: ArrayList<T>): ArrayList<K> {
        return list.map { item ->
            map(item)
        } as ArrayList<K>
    }

    fun reverseMap(list: ArrayList<K>): ArrayList<T> {
        return list.map { item ->
            reverseMap(item)
        } as ArrayList<T>
    }
}