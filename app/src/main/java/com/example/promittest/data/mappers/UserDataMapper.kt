package com.example.promittest.data.mappers

import com.example.promittest.data.apiEntities.UserDataApiEntity
import com.example.promittest.data.base.BaseMapper
import com.example.promittest.domain.Entities.UserDataDomain

class UserDataMapper() : BaseMapper<UserDataApiEntity, UserDataDomain>(){
    override fun map(el: UserDataApiEntity): UserDataDomain {
        return UserDataDomain(
            id = el.id,
            email = el.email,
            firstName = el.firstName,
            lastName = el.lastName,
            avatarUrl = el.avatarUrl,
            avatar = null
        )
    }

    override fun reverseMap(el: UserDataDomain): UserDataApiEntity {
        return UserDataApiEntity(
            id = el.id,
            email = el.email,
            firstName = el.firstName,
            lastName = el.lastName,
            avatarUrl = el.avatarUrl
        )
    }

}